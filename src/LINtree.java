/*
  ########################################################################################################

  LINtree: building prefix tree from LIN codes
    
  Copyright (C) 2024  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.math.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class LINtree {

    //### constants  #################################################################
    final static     String VERSION = "0.1 (240208ac)     Copyright (C) 2024 Institut Pasteur";
    static final     String NOTHING = "N.o./.T.h.I.n.G";
    static final        int BUFFER  = 1<<16;
    static final BigDecimal BD100   = new BigDecimal(100);
    static final       char SEP     = ':';
    static final     String OPPAR   = "(";
    static final     String CLPAR   = ")";
    static final     String COMMA   = ",";
    static final     String COLON   = ":";
    static final        int MAX     = Integer.MAX_VALUE;

    //### io   #######################################################################
    static         String infile;
    static BufferedReader in;

    //### data   #####################################################################
    static           int     nbin; // no. bins
    static        double[]   bin;  // bin thresholds
    static           int     n;    // no. LIN codes
    static        String[]   lbl;  // tax names
    static           int[][] lin;  // transposed lin codes [nbin][n]
    static StringBuilder     nwk;  // LIN tree

    //### stuffs   ###################################################################
    static int i, j, x, y, last, bid_prev, bid_curr;
    static double d;
    static String line;
    static BigDecimal bd;
    static String[] as;
    static StringBuilder sb;
    static ArrayList<Integer> ali;
    static ArrayList<String> als;

    public static void main(String[] args) throws IOException {

	//############################################################################
	//############################################################################
	//### doc                                                                  ###
	//############################################################################
	//############################################################################
	if ( args.length < 1 ) {
	    System.out.println("");
	    System.out.println("LINtree v" + VERSION);
	    System.out.println("");
	    System.out.println(" USAGE:  LINtree <infile>");
	    System.out.println("");
	    System.exit(1);
	}

	//############################################################################
	//############################################################################
	//### reading infile                                                       ###
	//############################################################################
	//############################################################################
	infile = args[0];
	if ( ! (new File(infile)).exists() ) { System.err.println("[ERROR] file " + infile + " does not exist"); System.exit(1); }
	in = ( infile.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(infile)), BUFFER))) : Files.newBufferedReader(Path.of(infile));
	//## reading header (i.e. bins)  #############################################
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.startsWith("#") || line.length() == 0 ) { line = NOTHING; continue; }
	    break;
	}
	if ( line.equals(NOTHING) ) { System.err.println("[ERROR] no specified bin"); System.exit(1); }
	as = line.split("[^0-9\\.]+");  //## NOTE: bin thresholds are doubles seperated by any characters that are not digits nor dot
	nbin = as.length;               //## NOTE: no. bins
	bin = new double[nbin+1];
	j = -1;
	while ( ++j < nbin ) {
	    try { bd = new BigDecimal(as[j]); }
	    catch ( NumberFormatException e ) { System.err.println("[ERROR] incorrectly formatted bin threshold: " + as[j]); System.exit(1); }
	    bin[j] = bd.negate().add(BD100).doubleValue();
	    if ( (bin[j] < 0) || bin[j] > 100 ) { System.err.println("[ERROR] bin thresholds should be percentage values: " + as[j]); System.exit(1); }
	}
	bin[nbin] = 0; //## NOTE: to deal with identical LIN codes
	// System.out.println(Arrays.toString(as));
	// System.out.println(Arrays.toString(bin));
	//## reading LIN codes  ######################################################
	als = new ArrayList<String>();
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.startsWith("#") || line.length() == 0 ) { line = NOTHING; continue; }
	    als.add(line);
	}
	n = als.size();          //## NOTE: no. LIN codes
	if ( n < 3 ) { System.err.println("[ERROR] insufficient number of row(s): " + n); System.exit(1); }
	lbl = new String[n];
	lin = new int[nbin][n];
	i = -1;
	while ( ++i < n ) {
	    as = als.get(i).split("\\s+"); //## NOTE: splitting using blanks and/or tabs
	    last = as.length - 1;
	    // lbl := concatenation of the first col(s), except the last one dedicated to the LIN code
	    j = 0; sb = new StringBuilder(as[j]); while ( ++j < last ) sb = sb.append("_").append(as[j]);
	    lbl[i] = sb.toString();
	    // LIN code := integers seperated by any non-integer character(s)
	    as = as[last].split("[^0-9]+");
	    if ( as.length < nbin ) { System.err.println("[ERROR] row " + (i+1) + " too short: " + Arrays.toString(as)); System.exit(1); }
	    if ( as.length > nbin ) { System.err.println("[ERROR] row " + (i+1) + " too long: " + Arrays.toString(as)); System.exit(1); }
	    j = nbin;
	    while ( --j >= 0 ) {
		try { x = Integer.parseInt(as[j]); }
		catch ( NumberFormatException e ) { System.err.println("[ERROR] row " + (i+1) + " incorrectly formatted at bin " + (j+1) + ": " + Arrays.toString(as)); System.exit(1); }
		lin[j][i] = x;
	    }
	}
	// System.out.println(Arrays.toString(lbl));
	// j = -1; while ( ++j < nbin ) System.out.println(Arrays.toString(lin[j]));
	
	//############################################################################
	//############################################################################
	//### building LIN tree                                                    ###
	//############################################################################
	//############################################################################
	ali = new ArrayList<Integer>(n); i = -1; while ( ++i < n ) ali.add(i);
	nwk = new StringBuilder( getClade(ali, 0) );
	// System.out.println(nwk.toString());

	// here nwk is the rooted tree topology, where each branch length is the bin id corresponding to its height
	//
	// ### EXAMPLE ###
	//
	// ---------------------------------------------------------------------------
	// %similarity     3.02  6.99  69.79 93.16 98.41 98.88 99.36 99.68 99.84 99.99
	// %dissimilarity  96.98 93.01 30.21 6.84  1.59  1.12  0.64  0.32  0.16  0.01
	// --------------  -----------------------------------------------------------
	// lbl    lbl ids  0     1     2     3     4     5     6     7     8     9    <= bin ids
	// ---    -------  -----------------------------------------------------------
	// A      0        0     0     0     0     0     0     0     0     0     0
	// B      1        0     1     0     0     0     0     0     0     0     0
	// C      2        0     1     0     0     0     0     1     0     0     0
	// D      3        0     1     1     0     0     0     0     0     0     0
	// E      4        0     3     0     0     0     0     0     0     0     0
	// F      5        0     3     0     0     0     0     1     0     0     0
	// ---------------------------------------------------------------------------
	//
	// ==>  nwk = (A:1,((B:6,C:6):2,D:2):1,(E:6,F:6):1)
	//
	// in consequence:
	// + for each leaf, the associated branch length is the %dissimilarity corresponding to the specified bin id
	//   e.g.  "A:1" => "A:93.01"  
	// + for each internal branch, the associated length is the difference between the %dissimilarity on each side of the corresponding closing parenthesis
	//   e.g.  "C:6):2" => "C:0.64):29.57"    30.21 - 0.64 = 29.57

	//## replacing every bin id height with branch length  #######################
	i = -1;
	while ( (i=1+nwk.indexOf(":", ++i)) > 0 ) {
	    x = nwk.indexOf(COMMA, i); x = ( x < 0 ) ? MAX : x;
	    y = nwk.indexOf(CLPAR, i); y = ( y < 0 ) ? MAX : y;
	    j = ( x < y ) ? x : y;
	    bid_curr = Integer.parseInt(nwk.substring(i, j));
	    if ( nwk.charAt(i-2) != ')' )    //## NOTE: terminal branch
		nwk = nwk.replace(i, j, String.valueOf(bin[bid_curr]));
	    else {                           //## NOTE: internal branch
		bd = BigDecimal.valueOf(bin[bid_curr]);
		line = bd.subtract(BigDecimal.valueOf(bin[bid_prev])).toString();
		nwk = nwk.replace(i, j, line);
	    }
	    bid_prev = bid_curr;
	}

	System.out.println(nwk.append(";").toString());
    }

    //##############################################################################################################
    //## returns the newick representation of the specified lbl ids according to the specified bin id             ##
    //##############################################################################################################
    private static String getClade(final ArrayList<Integer> lid, final int bid) {
	StringBuilder clade = new StringBuilder(OPPAR);
	int u = 0;
	//## only one leaf => returns that leaf
	if ( lid.size() == 1 ) return lbl[lid.get(0)];
	//## more than one leaf with identical LIN codes (bid == nbin) => creates a final clade
	if ( bid == nbin ) {
	    clade = clade.append(lbl[lid.get(u)]).append(COLON).append(String.valueOf(bid));
	    while ( ++u < lid.size() ) clade = clade.append(COMMA).append(lbl[lid.get(u)]).append(COLON).append(String.valueOf(bid));
	    return clade.append(CLPAR).toString();
	}
	//## more than one leaf with different LIN codes => computes the leaf partition 
	ArrayList<ArrayList<Integer>> partid = getPartition(lid, bid);
	//## only one partition => next bin
	if ( partid.size() == 1 ) return getClade(lid, bid+1);
	//## more than one partition => creates a clade, and for each child => new bin
	clade = clade.append(getClade(partid.get(u), bid+1)).append(COLON).append(String.valueOf(bid));
	while ( ++u < partid.size() ) clade = clade.append(",").append(getClade(partid.get(u), bid+1)).append(COLON).append(String.valueOf(bid));
	return clade.append(CLPAR).toString();
    }

    //##############################################################################################################
    //## returns the partition of the specified lbl ids according to the LIN classes within the specified bin id  ##
    //##############################################################################################################
    private static ArrayList<ArrayList<Integer>> getPartition(final ArrayList<Integer> lid, final int bid) {
	final int sz = lid.size();
	int u, v, pid;
	final String[] pi = new String[sz];
	//## storing every pair (pid,lid) into pi, and next sorting
	u = sz; while ( --u >= 0 ) pi[u] = pair(lin[bid][lid.get(u)], lid.get(u)); //## NOTE: pi[u] = pid":"lid
	Arrays.sort(pi);
	//## building alali such that alali.get(v) contains the lbl ids from the same partition v
	ArrayList<ArrayList<Integer>> alali = new ArrayList<ArrayList<Integer>>(); //## NOTE: alali.get(v) == lbl ids belonging to partition v
	v = 0;
	alali.add(new ArrayList<Integer>());
	u = 0;
	pid = lft(pi[u]);
	alali.get(v).add(rgt(pi[u]));
	while ( ++u < sz ) {
	    if ( lft(pi[u]) != pid ) {
		pid = lft(pi[u]);
		alali.add(new ArrayList<Integer>());
		++v;
	    }
	    alali.get(v).add(rgt(pi[u]));
	}
	return alali;
    }

    //##############################################################################################################
    //## return the String representation of the pair left'SEP'right                                              ##
    //##############################################################################################################
    private static String pair(final int left, final int right) { return String.valueOf(left) + SEP + String.valueOf(right); }

    //##############################################################################################################
    //## return the left/right integer part of the specified pair := int'SEP'int                                  ##
    //##############################################################################################################
    private static int lft(final String pair) { return Integer.parseInt(pair.substring(0, pair.indexOf(SEP))); }
    private static int rgt(final String pair) { return Integer.parseInt(pair.substring(pair.indexOf(SEP)+1)); }

}
