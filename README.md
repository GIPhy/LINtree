[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/)
[![JAVA](https://img.shields.io/badge/Java-11-be0032?logo=java)](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

# LINtree

_LINtree_ is a command line tool written in [Java](https://docs.oracle.com/en/java/) to infer prefix trees with branch lengths from sets of Life Identification Number (LIN) codes (Marakeby et al. 2014; Vinatzer et al. 2017; Tian et al. 2020).
Such LIN-based prefix trees are very useful to reflect the (phylogenetic) relationships among the encoded genomes (e.g. Hennart et al. 2022).



## Installation

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/LINtree.git
```


## Compilation and execution

The source code of _LINtree_ is inside the _src_ directory. It requires **Java 11** (or higher) to be compiled.

#### Building an executable jar file

On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (**11** or higher) installed, a Java executable jar file can be created by typing the following command lines inside the _src_ directory:

```bash
javac LINtree.java
echo Main-Class: LINtree > MANIFEST.MF
jar -cmvf MANIFEST.MF LINtree.jar LINtree.class
rm MANIFEST.MF LINtree.class
```

This will create the executable jar file `LINtree.jar` that can be run with the following command line model:

```bash
java -jar LINtree.jar <infile>
```

#### Building a native code binary

On computers with [GraalVM](https://www.graalvm.org/downloads/) installed, a native executable can be built. In a command-line window, go to the _src_ directory, and type:

```bash
javac LINtree.java
native-image LINtree LINtree
rm -f LINtree.build_artifacts.txt LINtree.class
```

This will create the native executable `LINtree` that can be run with the following command line model:
```bash
./LINtree <infile>
```


## Input file and output formats

The input file should first contain a line specifying the different similarity threshold percentage values (i.e. double values between 0 and 100). Each threshold value should be separated by at least one non-digit character (generally blank spaces).

Next, the input file should contain the LIN code block, each code being determined by one line where entries are separated by blank and/or tab character(s). The first entry(ies) is/are considered as the label (multiple entries are concatenated to obtain a unique label), whereas the last entry should contain the LIN code. A LIN code is defined by (positive) integers separated by any non-integer (and non-blank) character(s). Of important note, the number of integers determining the LIN code should be identical to the number of similarity thresholds specified at the begining of the input file.

Below is a very short representative example of input file:

```
 60  80.0  90  95  99.5
 genome 1    0.0.0.0.0
 geno seq 2  0_1_0_0_0
 genome3     1-0-0-0-0
 genome IV   0.0.0[1]0
```

Empty lines and lines starting with the `#` character (e.g. comments) are not considered when reading the input file.

The prefix tree is outputted to `stdout` in [NEWICK](https://en.wikipedia.org/wiki/Newick_format) format, with branch lengths computed from the specified similarity thresholds.



## Examples

The directory _example/_ contains two LIN code datasets (input LIN code file and inferred prefix tree).

##### _Ebolavirus_

The first dataset is derived from the ANI-based LIN coding of 24 _Ebolavirus_ genomes (Weisberg et al. 2015).
The prefix tree (_EBOV.nwk_) returned by _LINtree_ from the input file (_EBOV.txt_) is represented below.

![EBOV](example/EBOV.png "EBOV")

##### _Klebsiella_

The second dataset is derived from the cgMLST-based LIN coding of 44 _Klebsiella_ genomes (Hennart et al. 2022).
The prefix tree (_Kp.nwk_) returned by _LINtree_ from the input file (_Kp.txt_) is represented below.

![Kp](example/Kp.png "Kp")


## References

Hennart M, Guglielmini J, Bridel S, Maiden MCJ, Jolley KA, Criscuolo A, Brisse S (2022) _A Dual Barcoding Approach to Bacterial Strain Nomenclature: Genomic Taxonomy of Klebsiella pneumoniae Strains._ **Molecular Biology and Evolution**, 39(7):msac135. [doi:10.1093/molbev/msac135](https://doi.org/10.1093/molbev/msac135)

Marakeby H, Badr E, Torkey H, Song Y, Leman S, Monteil CL, Heath LS, Vinatzer BA (2014) _A System to Automatically Classify and Name Any Individual Genome-Sequenced Organism Independently of Current Biological Classification and Nomenclature._ **PLoS ONE**, 9(2):e89142. [doi:10.1371/journal.pone.0089142](https://doi.org/10.1371/journal.pone.0089142)

Tian L, Huang C, Mazloom R, Heath LS, Vinatzer BA (2020) _LINbase: a web server for genome-based identification of prokaryotes as members of crowdsourced taxa._ **Nucleic Acids Research**, 48(W1):W529-W537. [doi:10.1093/nar/gkaa190](https://doi.org/10.1093/nar/gkaa190)

Vinatzer BA, Tian L, Heath LS (2017) _A proposal for a portal to make earth’s microbial diversity easily accessible and searchable._ **Antonie van Leeuwenhoek**, 110(10):1271-1279. [doi:10.1007/s10482-017-0849-z](https://doi.org/10.1007/s10482-017-0849-z)

Weisberg AJ, Elmarakeby HA, Heath LS, Vinatzer BA (2015) _Similarity-based codes sequentially assigned to ebolavirus genomes are informative of species membership, associated outbreaks, and transmission chains._ **Open Forum Infectious Disease**, 12;2(1):ofv024. [doi:10.1093/ofid/ofv024](https://doi.org/10.1093/ofid/ofv024)



